var app = getApp();
var locationObj = {};
const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}


function isEmptyStr(str) {
  if (str == null || typeof str == "undefined" || str == "") {
    return true;
  }
  return false;
}

function isEmptyObj(obj) {
  if (obj == null || typeof obj == "undefined") {
    return true;
  } else {
    for (var key in obj) {
      return false;
    }
    return true;
  }
  return true;
}

function getScreenObj() {
  var screenObj = {};
  wx.getSystemInfo({
    success: function (res) {
      screenObj.screenWidth = res.windowWidth;
      screenObj.screenHeight = res.windowHeight;
    }
  });
  return screenObj;
}


function formateDate(date, flag) {
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var day = date.getDate();
  var hour = date.getHours();
  var minute = date.getMinutes();
  var second = date.getSeconds();
  if (month < 10) {
    month = "0" + month;
  }
  if (day < 10) {
    day = "0" + day
  }
  if (hour < 10) {
    hour = "0" + hour
  }
  if (minute < 10) {
    minute = "0" + minute
  }
  if (second < 10) {
    second = "0" + second
  }
  if (flag) {
    return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second
  } else {
    return year + "-" + month + "-" + day
  }
}

function decimalAdjust(type, value, exp) {
  // If the exp is undefined or zero...
  if (typeof exp === 'undefined' || +exp === 0) {
    return Math[type](value);
  }
  value = +value;
  exp = +exp;
  // If the value is not a number or the exp is not an integer...
  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
    return NaN;
  }
  // Shift
  value = value.toString().split('e');
  value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
  // Shift back
  value = value.toString().split('e');
  return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
}


function math_round10(value, exp) {
  return decimalAdjust('round', value, exp);
}

function math_floor10(value, exp) {
  return decimalAdjust('floor', value, exp);
}

function math_ceil10(value, exp) {
  return decimalAdjust('ceil', value, exp);
}

function showLoginError() {
  var msg = "获取用户信息失败！";
  switch (app.globalData.loginErrorStatus) {
    case 0:
      msg = "登录失败！";
      break;
    case 1:
      msg = "获取openId失败！";
      break;
    case 2:
      msg = "网络异常！";
      break;
    case 3:
      msg = "获取小程序用户信息失败！";
      break;
    case 4:
      msg = "获取会员信息失败！";
      break;
    default:
      break;
  }
  wx.showModal({
    title: '提示',
    content: msg,
    showCancel: false,
    success: function (res) {
    }
  })
}


function showToast(title, icon) {
  wx.showToast({
    title: "" + title,
    icon: icon,
    duration: 2000
  })
}

function hideToast() {
  wx.hideToast()
}



function getLocationObj(callback) {
  wx.getLocation({
    type: 'wgs84',
    success: function (res) {
      callback && callback(res);
    }
  })
}


function openLocation() {
  wx.getLocation({
    type: 'gcj02',
    success: function (res) {
      var latitude = res.latitude
      var longitude = res.longitude
      wx.openLocation({
        latitude: latitude,
        longitude: longitude,
        scale: 28
      })
    }
  })
}

function isPhone(value) {
  if (/^1(3|4|5|7|8)[0-9]\d{8}$/.test(value.trim())) return true;
  return false;
}


function isVerifyCode(value) {
  if (/^[0-9]\d{5}$/.test(value.trim())) return true;
  return false;
}

function mergeArray(arr1, arr2) {
  var _arr = new Array();
  for (var i = 0; i < arr1.length; i++) {
    _arr.push(arr1[i]);
  }
  for (var i = 0; i < arr2.length; i++) {
    var flag = true;
    for (var j = 0; j < arr1.length; j++) {
      if (arr2[i] == arr1[j]) {
        flag = false;
        break;
      }
    }
    if (flag) {
      _arr.push(arr2[i]);
    }
  }
  return _arr;
}

function isTabBarUrl(routerUrl) {
  var tabBarUrl = ["/pages/index/index", "/pages/signIn/signIn", "/pages/orderList/orderList", "/pages/myinfo/myinfo"];
  var currentUrl = routerUrl.split("?")[0];
  for (var i = 0; i < tabBarUrl.length; i++) {
    if (currentUrl == tabBarUrl[i]) {
      return true;
      break;
    }
  }
  return false;
}

module.exports = {
  getScreenObj: getScreenObj,
  isEmptyObj: isEmptyObj,
  isEmptyStr: isEmptyStr,
  formateDate: formateDate,
  math_round10: math_round10,
  math_floor10: math_floor10,
  math_ceil10: math_ceil10,
  formatTime: formatTime,
  showLoginError: showLoginError,
  showToast: showToast,
  hideToast: hideToast,
  getLocationObj: getLocationObj,
  isPhone: isPhone,
  isVerifyCode: isVerifyCode,
  mergeArray: mergeArray
} 
