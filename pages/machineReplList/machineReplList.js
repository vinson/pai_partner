var util = require('../../utils/util.js')
var app = getApp();
Page({
	data: {
		vipInfo:null,
		list:[],
		machineList:[],
		selectIndex:0,
		isLoading:false,
		isLoadingComplete:false,
		currPage:1,
		pageSize:20,
		showLoading: "none",
        showContent: true,
        noDataShow: false,
        noDataMsg:"暂无数据！"
	},
	onLoad: function() {
		this.init()
	},
	init: function(refresh) {
        var that = this;
        //调用应用实例的方法获取全局数据
        app.init(function() {
            //更新数据
            var vipInfo = app.globalData.vipInfo;
            that.setData({
                vipInfo: vipInfo
            })
            that.getMachineList();
        }, function() {
            util.showLoginError();
        },refresh)
    },
	onHide: function () {
        // 调用wx.navigateBack跳转的页面不会执行这个方法，只有页面真正的退出到别的页面才会调用
        try {
            wx.removeStorageSync(app.globalData.machineCheckKey)
        } catch (e) {
          // Do something when catch error
        }
    },
	onShow: function() {
		var checkRefresh = false;
		try {
			checkRefresh = wx.getStorageSync(app.globalData.machineCheckKey);
		} catch (e) {
		}
		if (checkRefresh) {
			this.setData({isLoadingComplete: false,currPage:1, list: [],totalCount:0, noDataShow: false, showContent: true, noDataMsg: "暂无数据！"});
            this.getMechineList();
		}
	},
	
    getMachineList:function(){
   	  	var that = this;
      	wx.getStorage({
	      	key: 'machineList',
	      	success: function(res) {
	        	var machineList = res.data;
	        	that.setData({machineList:machineList});
	        	that.getMachineReplList();
	      	} 
    	})
   },	

	getMachineList1234r: function() {
		var that = this;
		console.log("getMachineList");
		wx.request({
			url:app.globalData.httpRequest + "api/machine/info",
			method:"get",
			success: function(res) {
				if (res.data.status == 0) {
					console.log(res.data.data);
					that.setData({machineList:res.data.data});
				}
				that.getList();
			},
			fail: function() {
				that.getList();
			}
		})
	},
	bindPickerChange: function(e) {
		console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
            selectIndex: e.detail.value,
            list:[],
            totalCount:0,
            currPage:1
        });
        this.getMachineReplList();
	},
	getMachineReplList: function() {
		var that = this;
		var machineId = "";
		var machineList = this.data.machineList;
		var selectIndex = this.data.selectIndex;
		var vipInfo = that.data.vipInfo;
		if(vipInfo == null ){
			return;
		}
		if (machineList.length > 0) {
			machineId = machineList[selectIndex].MachineId;
		}
		this.setData({isLoading : true});
		var offset = (this.data.currPage - 1) * this.data.pageSize;
		wx.request({
			url:app.globalData.httpRequest + "/api/pai/repl/list/"+vipInfo.UserId,
			method:"get",
			data:{
				machineId:machineId,
				limit:that.data.pageSize,
				offset:offset 
			},
			success: function(res) {
				var status = res.data.status;
				var totalCount = that.data.totalCount;
				var list = that.data.list;
				if (status == 0) {
					list = list.concat(that.formatData(res.data.data));
					totalCount = that.data.currStatus > 2 ? parseInt(res.data.count) : list.length;
				}
				var noDataShow = totalCount == 0 ? true : false;
				var showContent = totalCount > 0 ? true : false;
				var noDataMsg = totalCount == 0 ? "暂无数据!" : "";
				var isLoadingComplete = (list.length == totalCount && totalCount > 0 ) ? true : false;
				that.setData({ isLoading: false, isLoadingComplete: isLoadingComplete, list: list, totalCount:totalCount, noDataShow: noDataShow, showContent: showContent, noDataMsg: noDataMsg});
			},
			fail: function() {
				var totalCount = that.data.totalCount;
				var noDataShow = totalCount == 0 ? true : false;
				var showContent = totalCount > 0 ? true : false;
				var noDataMsg = totalCount == 0 ? "网络异常，获取数据失败！" : "";
				that.setData({isLoading: false, noDataShow: noDataShow, showContent: showContent, noDataMsg:noDataMsg});
			}
		})
	},

	formatData: function(data) {
		var arr = [];
		data.forEach(function(item) {
			item.StatusText = item.Status ? "已审核":"未审核";
			var dateArr = item.ReplTime.split(" ");
			var date = "";
			var times = "";
			if (dateArr.length > 1) {
				times = dateArr[1];
			}
			if (dateArr.length > 0) {
				date = dateArr[0];
				var dateList = date.split("-");
				var year = dateList.length > 0 ? dateList[0] + "年" : "";
				var month = dateList.length > 1 ? dateList[1] + "月" : "";
				var day = dateList.length > 2 ? dateList[2] + "日" : "";
				date  = year + month + day;
			}
			item.ReplTime = date + " " + times;
			arr.push(item);
		})
		return arr;
	},
	onReachBottom: function () {
    	var that = this;  
	    if (!that.data.isLoading && !that.data.isLoadingComplete){  
	      that.setData({  
	        currPage: that.data.currPage + 1
	      });  
	      that.getMachineReplList();  
	    }  
    },
    onPullDownRefresh: function () {
        this.setData({isLoadingComplete: false, machineList: [], selectIndex:0, currPage:1, list: [],totalCount:0, noDataShow: false, showContent: true, noDataMsg: "暂无数据！"});
        console.log("onPullDownRefresh");
        this.getMachineList();
    },


	gotoDetail: function(e) {
		console.log(e);
		var index = e.currentTarget.dataset.index;
		var list = this.data.list;
		var item = list[index];
		var tradeNo = item.TradeNo;
		wx.navigateTo({
            url:"/pages/machineRepl/machineRepl?tradeNo=" + tradeNo + "&status=" + item.Status + "&replDate=" + item.ReplTime
        })
	}
})