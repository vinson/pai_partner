// pages/merchant/merchantOrdersList/merchantHomePage/merchantHomePage.js
var app = getApp();
var moment = require("../../../utils/moment.min.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: app.globalData.userInfo,
    vipInfo: app.globalData.vipInfo,
    imgPrex: app.globalData.imgPrex,
    isIpx:app.globalData.isIpx,
    // 当前设备宽度
		screenWidth:0,
		// 当前设备高度
    screenHeight:0,
    merchantId: '',
    // 保存请求到的首页数据
    merchantData: null,
    // 扫码后返回的订单详情
    scanAfterBackOrderDetail: null,
    // 控制订单确认弹框
    isShowModal: false,
    // 扫码成功弹框动画
    animationSuccess: {}, 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取屏幕宽高
    this.getScreenWidth();
    this.init();    
  },

  // 获取设备宽高
	getScreenWidth:function(){
		var that = this;
		// 获取设备信息
		wx.getSystemInfo({
			success: function(res) {
				var screenHeight = res.windowHeight;
        var isIpx = that.data.isIpx;        
				if(isIpx && screenHeight == 676){
					screenHeight -= 34;
				}
				that.setData({screenWidth:res.windowWidth,screenHeight:screenHeight});
			}
		})
  },

  init: function(){
    var that = this;
    //调用应用实例的方法获取全局数据
    app.init(function () {//更新数据
      var vipInfo = app.globalData.vipInfo;
      var userInfo = app.globalData.userInfo;
      that.setData({
        vipInfo: vipInfo,
        userInfo:userInfo
      })
      // 请求商家首页数据
      that.getMerchantData();
    }, function () {
      util.showLoginError();
    })
  },

  /**
   * 请求商家首页数据
   * 参数：商家id
   */
  getMerchantData: function(){
    var that = this;
    var userIdentity = wx.getStorageSync("userIdentity");
    this.setData({
      merchantId: userIdentity.merchantId,
    });
    wx.request({
      url: app.globalData.httpRequest + '/api/buy/store/supplier_info',
      header: {
        'content-type': 'application/x-www-form-urlencoded', // 默认值
      },
      method: 'get',
      data: { "supplierId": userIdentity.merchantId },
      success: function (res) {
        var status = res.data.status;
        if (status == 0) {
          var data = res.data.data;
          that.setData({
            merchantData: data,
          });
        } else if (status == 1) {
          var desc = res.data.desc;
          wx.showToast({
            title: desc,
            icon: "none"
          });
        }
      },
      fail: function () {
        wx.showToast({
          title: "网络错误，请重新刷新！",
          icon: "none"
        });
      }
    })
  },

  // 点击扫描二维码图片
  scanQcodeHandle: function(){
    var that = this;

    wx.scanCode({
      onlyFromCamera: false,
      scanType: ['qrCode','barCode','datamatrix','pdf417'],
      success: res => {
        var qrCode = res.result;
        var merchantId = that.data.merchantId;
        wx.request({
          url: app.globalData.httpRequest + '/api/buy/store/takeNo/detail',
          header: {
            'content-type': 'application/x-www-form-urlencoded', // 默认值
          },
          method: 'get',
          data: { "supplierId": merchantId, "takeNo": qrCode},
          success: function (res) {
            var status = res.data.status;
            
            if (status == 0) {
              wx.showLoading();
              var data = res.data.data;
              data.createTime = moment(data.OrderTime).format("YYYY-MM-DD HH:mm:ss");
              that.setData({
                scanAfterBackOrderDetail: data,
              });
              that.scanSuccessCall();
            } else if (status == 1) {
              var desc = res.data.desc;
              wx.showToast({
                title: desc,
                icon: "none"
              });
            }
          },
          fail: function () {
            wx.showToast({
                title: "网络错误，请重新刷新！",
                icon: "none"
              });
          }
        })
      },
      fail: err =>{

      }
    });
    
  },

  // 扫描成功后显示订单详情弹框
  scanSuccessCall: function(){
    wx.hideLoading();
    var that = this;
    this.scanSuccessAnimation = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    });
    this.setData({
      isShowModal: true,
    });
    that.scanSuccessAnimation.scale(0.8,0.8, 0.8).step();
    that.setData({
      animationSuccess: that.scanSuccessAnimation.export(),
    });

    setTimeout(function () {
      that.scanSuccessAnimation.scale(1,1,1).step();
      that.setData({
        animationSuccess: that.scanSuccessAnimation.export(),
      })
    }, 200);
  },

  // 确认订单按钮方法
  confirmButtonClick: function(){
    var that = this;
    var merchantId = this.data.merchantId;
    var orderId = this.data.scanAfterBackOrderDetail.Id;
    
    wx.request({
      url: app.globalData.httpRequest + '/api/buy/store/confirm_use',
      header: {
        'content-type': 'application/json', // 默认值
      },
      method: 'post',
      data: { "SupplierId": merchantId, "OrderId": orderId},
      success: function (res) {
        var status = res.data.status;
        
        if (status == 0) {
          that.scanSuccessAnimation.scale(0,0, 0).step();
          that.setData({
            animationSuccess: that.scanSuccessAnimation.export(),
          });
          that.getMerchantData();
          setTimeout(function () {
            that.setData({
              isShowModal: false,
            })
            wx.showToast({
              title: "订单确认成功！",
              icon: "success"
            });
          }, 200);
        } else if (status == 1) {
          var desc = res.data.desc;
          wx.showToast({
            title: desc,
            icon: "none"
          });
        }
      },
      fail: function () {
        wx.showToast({
          title: "网络错误，请重新刷新！",
          icon: "none"
        });
      }
    });
  },

  // 关闭订单确认弹框
  hideButtonClick: function(){
    var that = this;
    this.scanSuccessAnimation.scale(0,0, 0).step();
    this.setData({
      animationSuccess: this.scanSuccessAnimation.export(),
    });
    setTimeout(function () {
      that.setData({
        isShowModal: false,
      })
    }, 200);
  },

  // 注销按钮处理方法
  doLogOut: function(){
    wx.removeStorageSync("userIdentity");
    wx.switchTab({
      url: '/pages/index/index',
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  // onHide: function () {
  
  // },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getMerchantData();
    wx.showNavigationBarLoading() //在标题栏中显示加载
     //模拟加载
    setTimeout(function(){
      // complete
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
    },1500);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})