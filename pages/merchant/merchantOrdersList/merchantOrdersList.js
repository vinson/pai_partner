// pages/merchant/merchantOrdersList/merchantOrdersList.js
var app = getApp();
var moment = require("../../../utils/moment.min.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 当前设备宽度
		screenWidth:0,
		// 当前设备高度
    screenHeight:0,
    // 是否是iPhoneX
    isIpx:app.globalData.isIpx,
    // 七牛链接
    imgPrex:app.globalData.imgPrex,
    // 请求回来的未领取订单列表
    merchantNotReceiveOrderList: null,
    // 请求回来的已领取订单列表
    merchantToReceiveOrderList: null,
    // 临时id
    merchantId: '',
    // 控制是否显示加载数据
    isAddData: 3,   // 1 加载更多， 2 正在加载中， 3 没有更多数据
    // 保存当前状态 0:未领取 1：已领取
    currentOrderStatus: 0,
    // 当前的tab栏
    currentTab: 0,  
    // 当前窗口宽度
    winWidth: 0,  
    // 当前窗口高度
    winHeight: 0,  
    // 未领取订单数据总数
    notReceiveOrderCnt: 0,
    // 已领取订单数据总数
    toReceiveOrderCnt: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取屏幕宽高
    this.getScreenWidth();
    this.init();
  },

  init: function(){
    var that = this;
    this.initSystemInfo();
    var userIdentity = wx.getStorageSync("userIdentity");
    //调用应用实例的方法获取全局数据
    app.init(function () {//更新数据
      var vipInfo = app.globalData.vipInfo;
      var userInfo = app.globalData.userInfo;
      that.setData({
        vipInfo: vipInfo,
        userInfo:userInfo,
        merchantId: userIdentity.merchantId,
      });
      // 请求商家订单列表数据
      that.getMerchantOrderListData(userIdentity.merchantId);
    }, function () {
      util.showLoginError();
    })
  },

  // 获取设备宽高
	getScreenWidth:function(){
		var that = this;
		// 获取设备信息
		wx.getSystemInfo({
			success: function(res) {
				var screenHeight = res.windowHeight;
        var isIpx = that.data.isIpx;        
				if(isIpx && screenHeight == 676){
					screenHeight -= 34;
				}
				that.setData({screenWidth:res.windowWidth,screenHeight:screenHeight});
			}
		})
  },

  initSystemInfo:function(){
    var that = this;  
    wx.getSystemInfo( {
      success: function( res ) {  
        that.setData( {  
          winWidth: res.windowWidth,  
          winHeight: res.windowHeight  
        });  
      }    
    });
  },

  /**
   * 请求商家订单列表
   * 参数1：是否是第一次请求数据  布尔值
   * 参数2：商家id  
   * 参数3：订单列表状态
   * 参数4：页容量
   * 参数5：页码偏移量
   */
  getMerchantOrderListData: function(merchantId, currentOrderStatus = 0, limit = 10, offset = 0){
    var that = this;
    wx.request({
      url: app.globalData.httpRequest + '/api/buy/store/order/list',
      header: {
        'content-type': 'application/x-www-form-urlencoded', // 默认值
      },
      method: 'get',
      data: { "id": merchantId, "status": currentOrderStatus, "limit": limit, "offset": offset },
      success: function (res) {
        var status = res.data.status;
        var cnt = res.data.cnt;
        
        if (status == 0) {
          var orderList = res.data.data;
          if(orderList != null){
            for (let i = 0; i < orderList.length; i++) {
              orderList[i].orderCreateTime = moment(orderList[i].OrderTime).format("YYYY-MM-DD HH:mm:ss");
            };
            if(currentOrderStatus == 0){
              var tempArr = orderList;
              
              that.setData({
                merchantNotReceiveOrderList: tempArr,
                currentOrderStatus: currentOrderStatus,
                notReceiveOrderCnt: cnt,
                isAddData: orderList.length === limit ? 1 : 3,
              })
            } else if(currentOrderStatus == 1){
              var tempArr = orderList;

              that.setData({
                merchantToReceiveOrderList: tempArr,
                currentOrderStatus: currentOrderStatus,
                isAddData: orderList.length === limit ? 1 : 3,
              })
            }
          } else {
            that.setData({
              currentOrderStatus: currentOrderStatus,
              isAddData: 3,
            })
          }
        } else if (status == 1) {
          var desc = res.data.desc;
          wx.showToast({
            title: desc,
            icon: "none"
          });
        }
      },
      fail: function () {
        wx.showToast({
          title: "网络错误，请重新刷新！",
          icon: "none"
        });
      }
    })
  },

  // 加载更多订单列表数据
  getMoreOrderList:function(id, limit = 10){
    var that = this;
    var orderListStatus = that.data.currentOrderStatus;

    var merchantNotReceiveOrderList = that.data.merchantNotReceiveOrderList;
    var merchantToReceiveOrderList = that.data.merchantToReceiveOrderList;

    var notReceiveOrderCnt = that.data.notReceiveOrderCnt;
    var toReceiveOrderCnt = that.data.toReceiveOrderCnt;

    var offset = 0;

    if(orderListStatus == 0){
      if(merchantNotReceiveOrderList.length < notReceiveOrderCnt){
        offset = merchantNotReceiveOrderList.length;
        that.setData({isAddData:2});
      }else {
        that.setData({isAddData:3});
        return;
      }
    } else if(orderListStatus == 1){
      if(merchantToReceiveOrderList.length< toReceiveOrderCnt){
        offset = merchantToReceiveOrderList.length;
        that.setData({isAddData:2});
      }else {
        that.setData({isAddData:3});
        return;
      }

    };

    wx.request({
      url: app.globalData.httpRequest + '/api/buy/store/order/list',
      header: {
          'content-type': 'application/x-www-form-urlencoded', 
      },
      data: { "id": id, "status": currentOrderStatus, "limit": limit, "offset": offset },
      success: function(res) {
        var status = res.data.status;
        if(status == 0){
          var orderList = res.data.data;
          var count = res.data.cnt;
          if(orderList != null){
            for (let i = 0; i < orderList.length; i++) {
              orderList[i].orderCreateTime = moment(orderList[i].OrderTime).format("YYYY-MM-DD HH:mm:ss");
            };
            if(orderListStatus == 0){   
              that.setData({
                merchantNotReceiveOrderList:merchantNotReceiveOrderList.concat(orderList),
                notReceiveOrderCnt:count,
                isAddData: orderList.length === limit ? 1 : 3,
              });
            }else if(orderListStatus == 1){
              that.setData({
                merchantToReceiveOrderList:merchantToReceiveOrderList.concat(orderList),
                notReceiveOrderCnt:count,
                isAddData: orderList.length === limit ? 1 : 3,
              });
            }
          }
        }else if(status == 1){
          util.showToast(res.data.desc, "none");
        }
      },
      fail: function(res) { 
        util.showToast(app.globalData.NETWORK_EXCEPTION,"none");
        that.setData({isAddData: 3});
      }
    })
  },

  // 监听轮播图变化
  bindChange: function(e) {  
    var that = this;  
    var merchantId = this.data.merchantId;
    that.setData({status: e.detail.current});  
    that.getMerchantOrderListData(merchantId, e.detail.current);
  },

  // 点击顶部栏切换
  swichNav: function(e) {  
    var that = this;
    if( that.data.currentOrderStatus == e.currentTarget.dataset.current ) {  
      return ;
    } else { 
      var current = e.currentTarget.dataset.current;
      var status = e.currentTarget.dataset.status;
      var merchantId = this.data.merchantId;
      that.setData({
        currentTab: parseInt(current),
        currentOrderStatus: parseInt(status),
      });
      that.getMerchantOrderListData(merchantId, status);
      //没有数据就进行加载
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  scrollToLowerHandle:function(){
    this.getMoreOrderList();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh:function(){
    var { merchantId } = this.data;
    this.getMoreOrderList( merchantId );
    wx.showNavigationBarLoading() //在标题栏中显示加载
     //模拟加载
    setTimeout(function(){
      // complete
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
    },1500);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    //在标题栏中显示加载动画
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})