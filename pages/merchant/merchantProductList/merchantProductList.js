// pages/merchant/merchantOrdersList/merchantProductList/merchantProductList.js
var app = getApp();
var util = require("../../../utils/util.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgPrex: app.globalData.imgPrex,
    productList: [],
    // 控制loading状态
    isLoading:0,
    merchantId: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.init();
  },

  init: function(){
    var that = this;
    
    var userIdentity = wx.getStorageSync("userIdentity");
    //调用应用实例的方法获取全局数据
    app.init(function () {//更新数据
      var vipInfo = app.globalData.vipInfo;
      var userInfo = app.globalData.userInfo;
      that.setData({
        vipInfo: vipInfo,
        userInfo:userInfo,
        merchantId: userIdentity.merchantId,
      })
      // 请求商家首页数据
      that.getMerchantProductList(userIdentity.merchantId);
    }, function () {
      util.showLoginError();
    })
  },

  // 请求商家商品列表
  getMerchantProductList:function(merchantId){
    var that = this;
    wx.showLoading();
    that.setData({isLoading:1})
    wx.request({
      url: app.globalData.httpRequest + '/api/buy/store/product/list',
      method: "get",
      header: {
        'content-type': 'application/x-www-form-urlencoded',
      },
      data: { supplierId: merchantId, userId: "0"},
      success: function (res) {
        var status = res.data.status;
        if (status == 0) {
          var productList = res.data.data.ProductList;
          
          if(productList != null){
             that.setData({productList:productList})
          }else {
             that.setData({productList:[]})
          }
        } else if (status == 1) {
          var desc = res.data.desc;
          util.showToast(desc, "none"); 
        }
         wx.hideLoading();
      },
      fail: function () {
        wx.hideLoading();
        util.showToast(app.globalData.NETWORK_EXCEPTION, "none"); 
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})