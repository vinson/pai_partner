//获取应用实例
const app = getApp();
var util = require("../../utils/util.js");
Page({
    data: {
      vipInfo:null,
      orderPeople:0
    },
  
    onLoad: function() {
      wx.hideTabBar();
       this.init();
    },

  init: function () {
    var that = this;
    //调用应用实例的方法获取全局数据
    app.init(function () {//更新数据
      var vipInfo = app.globalData.vipInfo;
      that.setData({
        vipInfo: vipInfo
      })
      that.getMachineList();
      that.getOrderPeople();
    }, function () {
      util.showLoginError();
    })
  },
  getOrderPeople:function(){
    var that = this;
    var vipInfo = that.data.vipInfo;
    if (vipInfo == null) {
      return;
    }
    wx.request({
      url: app.globalData.httpRequest + '/api/partner/order_people/' + vipInfo.UserId,
      header: {
        'content-type': 'application/x-www-form-urlencoded', // 默认值
        'authorization': 'Token token=' + vipInfo.PaiToken
      },
      method: 'get',
      success: function (res) {
        var status = res.data.status;
        if (status == 0) {
          var orderPeople = res.data.data;
          that.setData({orderPeople:orderPeople})
        } else if (status == 1) {
          var desc = res.data.desc;
        }
      },
      fail: function () {
      }
    })
  },

  getMachineList: function () {
    var that = this;
    var vipInfo = that.data.vipInfo;
    if (vipInfo == null) {
      return;
    }
    wx.request({
      url: app.globalData.httpRequest + '/api/pai/repl/machine/' + vipInfo.UserId,
      header: {
        'content-type': 'application/x-www-form-urlencoded', // 默认值
        'authorization': 'Token token=' + vipInfo.PaiToken
      },
      method: 'get',
      success: function (res) {
        var status = res.data.status;
        if (status == 0) {
          var machineList = res.data.data;
          that.cacheMachineList(machineList);
          that.setData({ machineList: machineList })
        } else if (status == 1) {
          var desc = res.data.desc;
        }
      },
      fail: function () {
      }
    })
  },
  
  cacheMachineList: function (machineList) {
    wx.setStorage({
      key: "machineList",
      data: machineList
    })
  },

  paiMachineHandle:function(){
    wx.navigateTo({
      url:"/pages/machineList/machineList"
    })
  },

  paiMachineStockHandle:function(){
    wx.navigateTo({
      url:"/pages/machineStock/machineStock"
    })
  },

  paiMachineReplHandle:function(){
    wx.navigateTo({
      url:"/pages/machineReplList/machineReplList"
    })
  },
  
  orderListHandle:function(){
    wx.navigateTo({
      url:"/pages/orderList/orderList"
    })
  },

  // 注销按钮处理方法
  doLogOut: function(){
    wx.removeStorageSync("userIdentity");
    wx.switchTab({
      url: '/pages/index/index',
    });
  },

})