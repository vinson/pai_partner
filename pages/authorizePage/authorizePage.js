// pages/shop/authorizePage/authorizePage.js
const app = getApp();
var util = require("../../utils/util.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    flag:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var flag = options.flag
    this.setData({flag:flag})
  },

  getUserInfo:function(e){
     console.log(e)
     var encryptedData = e.detail.encryptedData;
     var iv = e.detail.iv;
     var sessionKey = app.globalData.sessionKey;
     var userInfo = e.detail.userInfo;
     this.getVipInfo(sessionKey, encryptedData, iv,userInfo);
  },


  getVipInfo: function(sessionKey, encryptedData, iv,userInfo) {
    var that = this;
    wx.request({
      url: app.globalData.httpRequest + '/api/pai/decryptsession',
      data: { sessionKey: sessionKey, encryptedData: encryptedData, iv: iv },
      header: {
          'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      method: 'post',
      success: function(res) {      
          if (res.data.status == 0) {
            var vipInfo = res.data.data;
            app.globalData.vipInfo = vipInfo;

            app.globalData.userInfo = userInfo;
            app.globalData.userInfo.session_key = sessionKey;
            wx.setStorage({
              key:"userInfo",
              data:app.globalData.userInfo,
              success:function(){
              }
            })     

            wx.setStorage({
              key:"vipInfo",
              data:vipInfo,
              success:function(){
              }
            })      
          } else {
              app.globalData.loginErrorStatus = 4;
          }

          wx.navigateBack({
            delta: 1
          })
      },
      fail: function() {
          console.log('getVipInfo fail');
          app.globalData.loginErrorStatus = 2;
      }
    })
  
  },

})