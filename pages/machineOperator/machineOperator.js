// pages/machineOperator/machineOperator.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  machineStockHandle:function(){  
    wx.navigateTo({
      url:'/pages/machineStock/machineStock'
    })
  },

  machineReplHandle:function(){
    wx.navigateTo({
      url:'/pages/machineReplList/machineReplList'
    })
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  
})