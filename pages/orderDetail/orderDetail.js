// pages/shop/shopBuyOrder/shopBuyOrder.js
var app = getApp();

var util = require('../../utils/util.js');
var orderId = "";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgPrex:app.globalData.imgPrex,
    isIpx:app.globalData.isIpx,
    vipInfo:null,
    productList:[]
  },



 /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    orderId = options.orderId;
  },

  onShow:function(){
     this.init();
  },

  init:function(){
    var that = this;
      //调用应用实例的方法获取全局数据
    app.init(function() {//更新数据
        var vipInfo = app.globalData.vipInfo;
        that.setData({
            vipInfo: vipInfo
        })
        that.getOrderDetailByOrderId(orderId);
        
    }, function() {
        util.showLoginError();
    })
  },

  getOrderDetailByOrderId:function(orderId){
    var that = this;
    var vipInfo = that.data.vipInfo;
    if(vipInfo == null ){
      return;
    }
    wx.request({
      url: app.globalData.httpRequest_1 + '/api/buy/order/detail',
      header: {
          'content-type': 'application/x-www-form-urlencoded', 
          'authorization':'Token token='+vipInfo.PaiToken
      },
      method:"get",
      data:{orderId:orderId},
      success: function(res) {
        var status = res.data.status;
        if(status == 0){
          var productList = res.data.data;
          if(productList != null ){
            that.setData({productList: productList})
          }else{
            that.setData({productList: []})            
          }
        }else if(status == 1){
          var desc = res.data.desc; 
          util.showToast(desc,"none");
        }
      },
      fail: function() {  
          util.showToast(app.globalData.NETWORK_EXCEPTION,"none");
      }
    })
  },


  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})