var app = getApp()
var util = require("../../utils/util.js");
Page({
	data: {
		vipInfo:null,
		titles:["一","二","三","四","五","六","七","八","九","十"],
		productList:[],
		tradeNo:"",
		status:-1
	},
	onLoad: function(e) {
		var tradeNo = e.tradeNo;
		var status = e.status;
		var replDate = e.replDate;
		var statusText = (status == 0 || status == "0") ? "未审核" : "已审核";
        this.setData({ tradeNo: tradeNo,status:status,replDate:replDate, statusText:statusText});
	},
	onShow: function(refresh) {
		var that = this;
		app.init(function() {
            //更新数据
            var vipInfo = app.globalData.vipInfo;
            that.setData({
                vipInfo: vipInfo
            })
        }, function() {
            util.showLoginError();
        },refresh);

		this.getSlotList();
	},

	onPullDownRefresh: function () {
        this.getSlotList();
    },
	getSlotList: function() {
		var that = this;
		var tradeNo = this.data.tradeNo;
		var vipInfo = that.data.vipInfo;
		if(vipInfo == null ){
			return;
		}
		wx.request({
			url:app.globalData.httpRequest + "/api/pai/repl/detail/"+vipInfo.UserId,
			method:"get",
			data:{tradeNo:tradeNo},
			success: function(res) {
				if (res.data.status == 0) {
					that.getProductInfo(res.data.data);
				}
				else {
					that.setData({productList:[]});
				}
			},
			fail: function() {
				that.setData({productList:[]});
			}
		})
	},
	getProductInfo: function(data) {
		var pids = data.map(function(item) {
			return item.ProductId;
		})
		var that = this;
		wx.request({
			url:app.globalData.httpRequest + "/api/pai/repl/product",
			method:"get",
			data:{pids:pids.join(",")},
			success: function(res) {
				var productList = data;
				if (res.data.status == 0) {
					var list = res.data.data;
					productList.forEach(function(item) {
						for(var i = 0, len = list.length; i < len; i++) {
							item.ProductName = "";
							item.ProductCode = "";
							item.PreviewPicUrl = "";
							if (item.ProductId == list[i].ProductId) {
								item.ProductName = list[i].ProductName;
								item.PreviewPicUrl = app.globalData.imgPrex + list[i].PreviewPicUrl;
								item.ProductCode = list[i].ProductCode;
								break;
							}
						}
					})
				}
				productList = that.formatData(productList);
				that.setData({productList:productList});

			},
			fail: function() {
				var productList = that.formatData(data);
				that.setData({productList:productList});
			}
		})
	},
	formatData: function(data) {
		var arr = [];
		data.sort(function(a, b) {
			return a.SlotId - b.SlotId;
		});
		data.forEach(function(item) {
			item.Amount = item.ReplAmount;
			var slotId = parseInt(item.SlotId);
			var index = parseInt(slotId / 10);
			if (index in arr) {
				arr[index].push(item);	
			}
			else {
				arr[index] = [item];
			}
		})
		return arr;
	},
	gotoUpdate: function(e) {
		var item = e.currentTarget.dataset.index;
		var status = this.data.status;
		var machineReplShow = this.data.machineReplShow;
		// if (!machineReplShow) {
		// 	app.showErrorModal("无修改权限");
		// 	return;
		// }
		if (status != 0 && status != "0") {
			// app.showErrorModal("补货已完成，不可修改");
			return;
		}
		var tradeNo = this.data.tradeNo;
		item.TradeNo = tradeNo;
		try {
            wx.setStorageSync(app.globalData.machineReplItemKey,item);
        } catch (e) {    
        }
		wx.navigateTo({
            url:"/pages/machineReplUpdate/machineReplUpdate"
        })
	},
	save: function() {
		var status = this.data.status;
		var machineReplShow = this.data.machineReplShow;
		// if (!machineReplShow) {
		// 	app.showErrorModal("无审核权限");
		// 	return;
		// }
		if (status != 0 && status != "0") {
			app.showErrorModal("补货已完成，不可重复提交");
			return;
		}
		var that = this;
		wx.showModal({
			title: '提示',
			content: '是否确定审核补货单',
			success: function(res) {
				if (res.confirm) {
					that.confirmRepl();
				} else if (res.cancel) {
					console.log('用户点击取消')
				}
			}
		});		 
	},
	confirmRepl: function() {
		var tradeNo = this.data.tradeNo;
		var vipInfo = this.data.vipInfo
		if(vipInfo == null){
			return;
		}
		var userName = this.data.sellerInfo.UserName;
		wx.request({
            url:app.globalData.httpRequest + "/api/pai/repl/confirm/"+vipInfo.UserId,
            method:"post",
            data: {
                "tradeNo": tradeNo,
                "userName": vipInfo.NickName
            },
            header: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            success: function (res) {
                if (res.data.status == 0) {
                	try {
                        wx.setStorageSync(app.globalData.machineCheckKey, "true");
                    } catch (e) {
                    }
                    app.showErrorModal("审核补货单成功", function() {
                        wx.navigateBack();
                    });
                }
                else {
                    app.showErrorModal("系统繁忙，审核补货单失败");
                }
            },
            fail: function () {
                app.showErrorModal("网络异常，审核补货单失败");
            }
        }) 
	}
})