
var app = getApp();
var util = require("../../utils/util.js")
Page({  
  data: {  
    imgPrex:app.globalData.imgPrex,
    vipInfo:null,
    winWidth: 0,  
    winHeight: 0,  
    currentTab: 0,  
    status:0, //0:未签收 1：已签收
    allOrderList:[],
    noReceiveOrderList:[],

    allCount:0,
    noReceiveCount:0,
   

    isLoadMore:0, // 1:isloading 2
    showModalStatus:false,
    isLoadding:0,
    tabBarBadgeObj:{}

  },  
  
  onLoad: function(options) {  
    this.initSystemInfo();    
    this.init();
  },  
  onShow:function(){
    var that = this;
    wx.getStorage({
      key: 'status',
      success: function(res) {
        that.setData({status:res.data})
        that.getOrderListByStatus(res.data);
        wx.removeStorage({ key: 'status'});
      }
    })
  },
  init:function(){
    var that = this;
    var status = that.data.status;
        //调用应用实例的方法获取全局数据
    app.init(function() {//更新数据
        var vipInfo = app.globalData.vipInfo;
        that.setData({
            vipInfo: vipInfo
        })
        that.getOrderListByStatus(status);
    }, function() {
        util.showLoginError();
    })
  },
  getOrderStatus:function(){
    return this.data.status == 0 ? 0 : 1 ;
  },

  initSystemInfo:function(){
    var that = this;  
    wx.getSystemInfo( {
      success: function( res ) {  
        that.setData( {  
          winWidth: res.windowWidth,  
          winHeight: res.windowHeight  
        });  
      }    
    });  
  },



  bindChange: function(e) {  
    var that = this;  
    that.setData({status: e.detail.current});  
    that.getOrderListByStatus(e.detail.current);
  },  

  swichNav: function(e) {  
    var that = this;  
    if( that.data.status == e.currentTarget.dataset.current ) {  
      return ;  
    } else { 
      var current = e.currentTarget.dataset.current;
      var status = e.currentTarget.dataset.status;
      that.setData({
        currentTab: parseInt(current),
        status: parseInt(status),
      });
    // that.getOrderListByStatus(status);
      //没有数据就进行加载      
    }
  },


  getOrderListByStatus:function(orderListStatus){
    var that = this;
    //var orderListStatus = that.data.status;
    var vipInfo = that.data.vipInfo;
    if(vipInfo == null){
        return;
    }
    that.setData({isLoadding:1});
    wx.request({
    url: app.globalData.httpRequest_1 + '/api/buy/self_take/order/'+vipInfo.UserId,
    data:{status:orderListStatus,limit:8,offset:0},
    header: {
        'content-type': 'application/x-www-form-urlencoded', 
        'authorization':'Token token='+vipInfo.PaiToken
    },
    success: function(res) {
      var status = res.data.status;
      if(status == 0){
        var orderList = res.data.data;
        var count = res.data.count;
        if(orderList != null){
          for (var i = 0; i < orderList.length; i++) {
              var orderTime = orderList[i].OrderTime;
              orderList[i].OrderTime = util.formateDate(new Date(orderTime),true);
          }; 
          if(orderListStatus == 0){   
            that.setData({allOrderList:orderList,allCount:count});
            if(orderList.length == 0){
                that.showNoDataModal("open")
            }
          }else if(orderListStatus == 1){
            that.setData({noReceiveOrderList:orderList,noReceiveCount:count});
          }

          if(orderList.length == count){
            that.setData({isLoadMore:2});
          }
        }

      }else if(status == 1){
        var desc = res.data.desc;
        util.showToast(desc,"none")
      }
      that.setData({isLoadding:0});
    },
    fail: function(res) {  
        util.showToast(app.globalData.NETWORK_EXCEPTION,"none");
        that.setData({isLoadding:0});
    }
  })
  },

 getMoreOrderList:function(){
    var that = this;
    var orderListStatus = that.data.status;
    var vipInfo = that.data.vipInfo;
    var allOrderList = that.data.allOrderList;
    var noReceiveOrderList = that.data.noReceiveOrderList;
    var allCount = that.data.allCount;
    var noReceiveCount = that.data.noReceiveCount;
    var offset = 0;
    if(vipInfo == null){
        return;
    }

    if(orderListStatus == 0){
      if(allOrderList != null ){
        if(allOrderList.length< allCount){
          offset = allOrderList.length;
          that.setData({isLoadMore:1});
        }else {
          that.setData({isLoadMore:2});
          return;
        }
      }
    } else if(orderListStatus == 1){
      if(noReceiveOrderList != null){
        if(noReceiveOrderList.length< noReceiveCount){
          offset = noReceiveOrderList.length;
          that.setData({isLoadMore:1});
        }else {
          that.setData({isLoadMore:2});
          return;
        }
      }
    }

    wx.request({
      url: app.globalData.httpRequest_1 + '/api/buy/self_take/order/'+vipInfo.UserId,
      data:{status:orderListStatus,limit:8,offset:offset},
      header: {
          'content-type': 'application/x-www-form-urlencoded', 
          'authorization':'Token token='+vipInfo.PaiToken
      },
      success: function(res) {
        var status = res.data.status;
        if(status == 0){
          var orderList = res.data.data;
          var count = res.data.count;
          for (var i = 0; i < orderList.length; i++) {
              var orderTime = orderList[i].OrderTime;
              orderList[i].OrderTime = util.formateDate(new Date(orderTime),true);
          }
          if(orderListStatus == 0){   
            that.setData({allOrderList:allOrderList.concat(orderList),allCount:count});
            if(allOrderList.concat(orderList).length == count){
              that.setData({isLoadMore:2})
            }
          }else if(orderListStatus == 1){
            that.setData({noReceiveOrderList:noReceiveOrderList.concat(orderList),noReceiveCount:count});
            if(noReceiveOrderList.concat(orderList).length == count){
              that.setData({isLoadMore:2})
            }
          }
          
        }else if(status == 1){
       
        }
      },
      fail: function(res) { 
        util.showToast(app.globalData.NETWORK_EXCEPTION,"none");
        that.setData({isLoadMore:0});
      }
    })
  },

  orderDetailHandle:function(e){ 
    var that = this;
    var currentOrder = e.currentTarget.dataset.order;
    wx.navigateTo({
      url: '/pages/orderDetail/orderDetail?orderId='+currentOrder.OrderId
    })
  },

  receiveHandle:function(e){
    var that = this;
    var currentOrder = e.currentTarget.dataset.order;
    var currentIndex = e.currentTarget.dataset.currentindex;
    wx.showModal({
      title: '签收订单',
      content: '确定签收此订单吗',
      success: function(res) {
        if (res.confirm) {
          that.receiveOrder(currentOrder,currentIndex);
        } else if (res.cancel) {
        } 
      }
    })
  },

  receiveOrder:function(currentOrder,currentIndex){
    var that = this;
    var vipInfo = that.data.vipInfo; 
    var orderStatus = that.data.status;
    if(vipInfo == null) return; 
    wx.request({
      url: app.globalData.httpRequest_1 + '/api/buy/self_take/deliver/'+vipInfo.UserId,
      data:{deliverName:vipInfo.NickName,orderId:currentOrder.OrderId},
      header: {
          'content-type': 'application/x-www-form-urlencoded', 
          'authorization':'Token token='+vipInfo.PaiToken
      },
      method: 'post',
      success: function(res) {
        var status = res.data.status;
        if(status == 0){
          var allOrderList = that.data.allOrderList;
          var allCount= that.data.allCount;
          if(orderStatus == 0){
            allOrderList.splice(currentIndex,1);
            that.setData({ allOrderList: allOrderList,allCount:allCount-1});
          }
        }else if(status == 1){
          var desc = res.data.desc;
          util.showToast(desc,"none")
        }
      },
      fail: function(res) { 
        util.showToast(app.globalData.NETWORK_EXCEPTION,"none");
      }
    })
  },

  showNoDataModal:function () {  
    this.showModal("open");
  },  

  closeModal:function(){
    this.showModal("close"); 
  },

  modalHandle:function(e){
    console.log(e);
    var targetId = e.target.id;
    if(targetId == "modalScreen"){
        this.showModal("close");  
    }
  }, 

  showModal:function(currentStatus){  
    var that = this;
    var animation = wx.createAnimation({  
      duration: 200,  //动画时长  
      timingFunction: "linear", //线性  
      delay: 0  //0则不延迟  
    });  
    
    that.animation = animation;  
    animation.rotateX(0).scale3d(0.8,0.8,0.8).step();  
  
    that.setData({  
      animationData: animation.export()  
    })  
      
    setTimeout(function () {  
      animation.rotateX(0).scale3d(1,1,1).step();  
      that.setData({  
        animationData: animation  
      })  
        
      //关闭  
      if (currentStatus == "close") {  
        that.setData(  
          {  
            showModalStatus: false  
          }  
        );  
      }  
    }.bind(this), 200)  
    
    // 显示  
    if (currentStatus == "open") {  
      that.setData({  
        showModalStatus: true  
      });  
    }  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
     //this.getMoreOrderList();
  },
  scrollToLowerHandle:function(){
     this.getMoreOrderList();
  },

  onPullDownRefresh:function(){
    this.getOrderListByStatus(this.data.status);
    wx.showNavigationBarLoading() //在标题栏中显示加载
     //模拟加载
    setTimeout(function(){
      // complete
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
    },1500);
  },

})