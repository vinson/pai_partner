//获取应用实例
const app = getApp();
var util = require("../../utils/util.js");
Page({
  data: {
    userInfo: app.globalData.userInfo,
    vipInfo: app.globalData.vipInfo,
    partnerId:'',
    supplierId:''
  },

  onLoad: function(){
    var userIdentity = wx.getStorageSync("userIdentity");
    
    if(userIdentity && userIdentity.userType == 0){
      wx.redirectTo({
        url: '/pages/machineAdmin/machineAdmin',
      });
    } else if(userIdentity && userIdentity.userType == 1){
      wx.redirectTo({
        url: '/pages/merchant/merchantHomePage/merchantHomePage',
      });
    };
  },

  onShow: function() {
    wx.hideTabBar();
    this.init();
  },

  init: function () {
    var that = this;
    //调用应用实例的方法获取全局数据
    app.init(function () {//更新数据
      var vipInfo = app.globalData.vipInfo;
      that.setData({
        vipInfo: vipInfo
      })
    }, function () {
      util.showLoginError();
    })
  },

  // 点击派派机管理员按钮的处理方法
  paiMachineButtonHandle: function(){
    var that = this;
    var vipInfo = this.data.vipInfo;

    wx.request({
      url: app.globalData.httpRequest + '/api/partner/partner_info',
      header: {
        'content-type': 'application/x-www-form-urlencoded', // 默认值
      },
      method: 'get',
      data: { "userId": vipInfo.UserId},
      success: function (res) {
        var status = res.data.status;
        
        if (status == 0) {
          wx.setStorage({
            key: 'userIdentity',
            data: {
              userType: 0
            }
          });
          wx.redirectTo({
            url: '/pages/machineAdmin/machineAdmin',
          })
        } else if (status == 1) {
          var desc = res.data.desc;
          wx.showToast({
            title: desc,
            icon: "none"
          });
        }
      },
      fail: function () {
        wx.showToast({
          title: "网络错误，请重新刷新！",
          icon: "none"
        });
      }
    })
  },

  // 点击每日派派商家按钮的处理方法
  paiHomeButtonHandle: function(){
    var that = this;
    var vipInfo = this.data.vipInfo;

    wx.request({
      url: app.globalData.httpRequest + '/api/partner/supplier_info',
      header: {
        'content-type': 'application/x-www-form-urlencoded', // 默认值
      },
      method: 'get',
      data: { "userId": vipInfo.UserId},
      success: function (res) {
        var status = res.data.status;
        
        if (status == 0) {
          var data = res.data.data;
          wx.setStorage({
            key: 'userIdentity',
            data: {
              merchantId: data,
              userType: 1
            }
          });
          wx.redirectTo({
            url: '/pages/merchant/merchantHomePage/merchantHomePage',
          })
        } else if (status == 1) {
          var desc = res.data.desc;
          wx.showToast({
            title: desc,
            icon: "none"
          });
        }
      },
      fail: function () {
        wx.showToast({
          title: "网络错误，请重新刷新！",
          icon: "none"
        });
      }
    })
  }
})