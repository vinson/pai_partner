// pages/machineList/machineList.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    machinePeopleList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.init();
  },

  init: function () {
    var that = this;
    //调用应用实例的方法获取全局数据
    app.init(function () {//更新数据
      var vipInfo = app.globalData.vipInfo;
      that.setData({
        vipInfo: vipInfo
      })
      that.getMachinePeopleList();
    }, function () {
      util.showLoginError();
    })
  },

  getMachinePeopleList:function(){
    var that = this;
    var vipInfo = that.data.vipInfo;
    if (vipInfo == null) {
      return;
    }
    wx.request({
      url: app.globalData.httpRequest + '/api/partner/order_people_list/' + vipInfo.UserId,
      header: {
        'content-type': 'application/x-www-form-urlencoded', // 默认值
        'authorization': 'Token token=' + vipInfo.PaiToken
      },
      method: 'get',
      success: function (res) {
        var status = res.data.status;
        if (status == 0) {
            var machinePeopleList = res.data.data;
            that.setData({machinePeopleList:machinePeopleList})
        } else if (status == 1) {
          var desc = res.data.desc;
        }
      },
      fail: function () {
      }
    })
  },



  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})