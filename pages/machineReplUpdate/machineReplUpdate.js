var numbers = require('../../utils/numbers.js');
var util = require('../../utils/util.js');
var app = getApp();
Page({
	data: {
		startSlotId:"",
		endSlotId:"",
		slot:{}
	},
    onLoad: function() {
        var that = this;
        var startSlotId = 1;
        try {
            var data = wx.getStorageSync(app.globalData.machineReplItemKey);
            var slot = data ? data : "";
            startSlotId = slot.SlotId;
            that.setData({slot:slot});
        } catch (e) {    
        }
        this.setData({startSlotId:startSlotId});
    },
	onShow: function() {
        this.init();
    },
	init: function() {
        var that = this;
        var slot = this.data.slot;
        try {
            var product = wx.getStorageSync(app.globalData.machineProductKey);
            if (product) {
                slot.ProductId = product.ProductId;
                slot.ProductName = product.ProductName;
                slot.ProductNo = product.ProductNo;
                slot.PreviewPicUrl = product.Pic;
            } 
            that.setData({slot:slot});
        } catch (e) {    
        }
        try {
            wx.removeStorageSync(app.globalData.machineProductKey);
        } catch (e) {           
        }
        //调用应用实例的方法获取全局数据
        app.init(function() {
            //更新数据
            var vipInfo = app.globalData.vipInfo;
                that.setData({
                vipInfo: vipInfo
            })
        }, function() {
            util.showLoginError();
        })
    },
   
	onUnload: function () {
        // 调用wx.navigateTo跳转的页面不会执行这个方法，只有页面真正的退出到别的页面才会调用
        try {
            wx.removeStorageSync(app.globalData.machineReplItemKey)
        } catch (e) {
          // Do something when catch error
        }
    },
	inputStartSlotId: function(event) {
	},
	inputEndSlotId: function(event) {

	},
	startSlotBlur: function(event) {
		var value = event.detail.value;
		if (!numbers.isPositiveIntNum(value)) {
			app.showErrorModal("货道号大于等于1，小于等于58");
			value = 1;
		}
		value = parseInt(value);
		if (value > 58) {
			app.showErrorModal("货道号大于等于1，小于等于58");
			value = 58;
		}
		this.setData({startSlotId: value});

	},
	endSlotBlur: function(event) {
		var value = event.detail.value;
		if (!value) {
			if (numbers.isNonNegativeInt(value)) {
				app.showErrorModal("货道号大于等于1，小于等于58");
				value = 1;	
			}
		}
		else {
			if (!numbers.isPositiveIntNum(value)) {
				app.showErrorModal("货道号大于等于1，小于等于58");
				value = 1;
			}
		}
		value = parseInt(value);
		if (value > 58) {
			value = 58;
			app.showErrorModal("货道号大于等于1，小于等于58"); 
		}
		this.setData({endSlotId:value});
	},
    amountBlur: function(event) {
        var value = event.detail.value;
        var slot = this.data.slot;
        if (!numbers.isNonNegativeInt(value)) {
            app.showErrorModal("现存数量要为整数");
            value = 0;
        }
        value = parseInt(value);
        if (value > 7) {
            app.showErrorModal("现存数量最大为7");
            value = 7;
        }
        slot.Amount = value;
        slot.ReplAmount = value;
        this.setData({slot: slot});
    },
    choseProduct: function() {
        wx.navigateTo({
            url:"/pages/machineProduct/machineProduct"
        })
    },
    save: function() {
        var that = this;
        var slot = this.data.slot;
        var startSlotId = this.data.startSlotId;
        var endSlotId = this.data.endSlotId;
        if (!numbers.isPositiveIntNum(startSlotId)) {
            app.showErrorModal("前面货道号大于等于1，小于等于58");
            return ;
        }
        startSlotId = parseInt(startSlotId);
        if (startSlotId > 58) {
            app.showErrorModal("前面货道号大于等于1，小于等于58");
            return ;
        }

        if (!endSlotId) {
            if (numbers.isNonNegativeInt(endSlotId)) {
                app.showErrorModal("后面货道号大于等于1，小于等于58");
                return ;
            }
            endSlotId = startSlotId;
        }
        else {
            if (!numbers.isPositiveIntNum(endSlotId)) {
                app.showErrorModal("后面货道号大于等于1，小于等于58");
                return ;
            }
        }
        endSlotId = parseInt(endSlotId);
        if (endSlotId > 58) {
            app.showErrorModal("后面货道号大于等于1，小于等于58"); 
            return ;
        }
        if (startSlotId > endSlotId) {
            app.showErrorModal("前面货道号要小于后面货道号"); 
            return ;
        }
        var amount = slot.Amount;
        if (!numbers.isNonNegativeInt(amount)) {
            app.showErrorModal("现存数量要为整数");
            return ;
        }
        amount = parseInt(amount);
        if (amount > 7) {
            app.showErrorModal("现存数量最大为7");
            return ;
        }
        if (!slot.ProductId) {
            slot.ProductId = "4230888d-ebd3-493b-425b-84945ac8cc3c" // 神秘礼品
        }
        var detailList = [];
        for (var i = startSlotId; i <= endSlotId; i++) {
            if (i < 1 || i > 58 || i % 10 ==0 || i % 10 == 9) continue;
            detailList.push({
                SlotId:i + "",
                ProductId:slot.ProductId,
                ReplAmount:amount
            });
        }
        console.log(detailList);
        if (detailList.length == 0) {
            wx.navigateBack();
            return ;
        }
        that.updateReplDetail(slot.TradeNo,detailList);
    },
    updateReplDetail:function(slot,detailList){
        var vipInfo = this.data.vipInfo;
        if(vipInfo == null ){
            return;
        }
        wx.request({
            url:app.globalData.httpRequest + "/api/pai/repl/detail/update/"+vipInfo.UserId,
            method:"post",
            data: {
                "TradeNo": slot,
                "DetailList": detailList
            },
            header: {
                'content-type': 'application/json'
            },
            success: function (res) {
                if (res.data.status == 0) {
                    app.showErrorModal("更新补货单成功", function() {
                        wx.navigateBack();
                    });
                }
                else {
                    app.showErrorModal("系统繁忙，更新补货单失败");
                }
            },
            fail: function () {
                app.showErrorModal("网络异常，更新补货单失败");
            }
        })  
    }

})