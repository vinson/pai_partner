var app = getApp();
var util = require("../../utils/util.js");
Page({
	data: {
		vipInfo:null,
		totalStock:0,
		machineList:[],
        slotStockList:[],
        selectIndex:0
	},
	onShow: function() {
		this.init();
	},
	init: function(refresh) {
        var that = this;
        //调用应用实例的方法获取全局数据
        app.init(function() {
            //更新数据
            var vipInfo = app.globalData.vipInfo;
            that.setData({
                vipInfo: vipInfo
            })
            that.getMechineList();
        }, function() {
            util.showLoginError();
        },refresh)
    },

    getMechineList:function(){
        var that = this;
        wx.getStorage({
          key: 'machineList',
          success: function(res) {
            var machineList = res.data;
            that.setData({machineList:machineList});
            that.getMechineStockByMachineId(machineList[0].MachineId);
          } 
        })
    },

	getMechineStockByMachineId: function(machineId) {
		var that = this;
        var vipInfo = that.data.vipInfo;
        if (vipInfo == null) return ;
		wx.request({
			url: app.globalData.httpRequest + "/api/pai/repl/stock/"+vipInfo.UserId,
            method: 'get',
            data: { machineId: machineId},
            success: function(res) {  
                var status = res.data.status;    
                if(status == 0){   
                    var totalStock = 0;
                    var slotStockList = res.data.data;
                    slotStockList.forEach(function(item) {
                    	totalStock += parseInt(item.amount);
                    })    
                    that.setData({totalStock:totalStock, slotStockList:slotStockList});
                } else if(status == 1){
                    var desc = res.data.desc;
                    util.showToast(desc,"none");
                }
            },
            fail: function() {
                util.showToast(app.globalData.NETWORK_EXCEPTION,"none");
            }
		});
	},

    bindMachineChange:function(e){
        var machineList = this.data.machineList;
        var selectIndex = e.detail.value;
        this.setData({
            selectIndex: e.detail.value,
            slotStockList:[],
            totalStock:0
        });
        this.getMechineStockByMachineId(machineList[selectIndex].MachineId);
    }

})