var numbers = require('../../utils/numbers.js')
var app = getApp();
Page({
    data: {
        list: [],
        productName:"",
        showLoading: "none",
        showContent: "none",
        noDataShow: "block"
    },
    onLoad: function() {
    },
    scanProduct: function() {
        var that = this;
        wx.scanCode({
            success: function(res) {
                var productNo = res.result;
                that.getProductByCode(productNo);
            },
            fail: function(res) {
                var errMsg = res.errMsg;
                if (errMsg.indexOf("cancel") != -1) {
                    return;
                }
                app.showErrorModal("扫码失败！");
            }
        })
    },
    getProductByCode: function(productNo) {
        var that = this;
        this.setData({ showLoading: "block" });
        wx.request({
            url: app.globalData.httpRequest + "/api/pai/repl/product",
            method: 'get',
            data: {productCode:productNo},
            success: function(res) {
                that.setData({ showLoading: "none", noDataShow: "none", showContent: "block"});
                if (res.data.status == 0) {
                    var data = res.data.data;
                    var isExist = that.checkProductExist(data.ProductId);
                    var list = [];
                    if (!isExist) {
                        data.Pic = app.globalData.imgPrex + data.PreviewPicUrl;
                        data.OriginalDiscountPrice = data.DiscountPrice;
                        data.PriceText = "销售价";
                        list.push(data);
                        that.setData({ list: list });
                        wx.showToast({
                            title: "商品查询成功",
                            duration: 1500
                        })
                    } else {
                        app.showErrorModal("该商品已经存在");
                    }
                } else {
                    app.showErrorModal("没有相应的商品");
                }
            },
            fail: function() {
                that.setData({ showLoading: "none" });
                app.showErrorModal("网络异常，请重新扫码！");
            }
        })
    },
    getProductByName: function(productName) {
        var that = this;
        this.setData({ showLoading: "block" });
        wx.request({
            url: app.globalData.httpRequest + "/api/pai/repl/product",
            method: 'get',
            data: {productName:productName},
            success: function(res) {
                that.setData({ showLoading: "none", noDataShow: "none", showContent: "block"});
                if (res.data.status == 0) {
                    var data = res.data.data;
                    var list = [];
                    data.forEach(function(item) {
                        item.Pic = app.globalData.imgPrex + item.PreviewPicUrl;
                        item.OriginalDiscountPrice = item.DiscountPrice;
                        item.PriceText = "销售价";
                        list.push(item);
                    })
                    that.setData({ list: list });
                    wx.showToast({
                        title: "商品查询成功",
                        duration: 1500
                    })
                } else {
                    app.showErrorModal("没有相应的商品");
                    that.setData({ showLoading: "none", list:[] });
                }
            },
            fail: function() {
                that.setData({ showLoading: "none", list:[] });
                app.showErrorModal("网络异常，请重新搜索！");
            }
        })
    },
    checkItemExists: function(id, list) {
        var pos = false;
        for (var i = 0, len = list.length; i < len; i++) {
            if (id == list[i]) {
                pos = true;
                break;
            }
        }
        return pos;
    },
    checkProductExist: function(productId) {
        var list = this.data.list;
        var result = false;
        for (var i = 0, len = list.length; i < len; i++) {
            if (list[i].ProductId == productId) {
                result = true;
                break;
            }
        }
        return result;
    },
    clearAll: function() {
        this.setData({ list: [], showContent: "none", noDataShow: "block", productName:""});
    },
    searchProduct: function(event) {
        var value = event.detail.value;
        this.setData({ productName: value });
    },
    onSearch: function() {
        var productName = this.data.productName;
        if (!productName) return;
        this.getProductByName(productName);
    },
    choseProduct: function(event) {
        var index = event.currentTarget.dataset.index;
        var list = this.data.list;
        try {
            wx.setStorageSync(app.globalData.machineProductKey, list[index]);
            wx.navigateBack();
        } catch (e) {
            wx.navigateBack();
        }
    }
})
