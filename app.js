//app.js

var app = getApp();
var canUseSetting = false;
App({
    onLaunch: function(options) {
        var that = this;       
        that.versionUpdate();
        // 获取分享群shareTicket
        if (options.scene == 1044) {
            this.globalData.shareTicket = options.shareTicket;
        }
        
        if (!this.globalData.userInfo ) {
            this.getUserInfoCache()
        }

        if(!this.globalData.vipInfo){
            this.getVipInfoCache();
        }

        
        this.judgeIphoneX();
        wx.showTabBarRedDot({ index: 4 })
        canUseSetting = wx.canIUse('button.open-type.openSetting');
    },

    versionUpdate:function(){
        const updateManager = wx.getUpdateManager();
        updateManager.onCheckForUpdate(function (res) {
        })

        updateManager.onUpdateReady(function () {
          wx.showModal({
            title: '更新提示',
            content: '新版本已经准备好，是否重启应用？',
            success: function (res) {
              if (res.confirm) {
                updateManager.applyUpdate()
              }
            }
          })
        })

        updateManager.onUpdateFailed(function () {
          // 新的版本下载失败
          wx.showModal({
            title: '更新提示',
            content: '新版本下载失败',
            showCancel:false
          })
        })  
    },

    onShow: function(options) {
        var that = this;
        if (options.scene == 1044) {
            this.globalData.shareTicket = options.shareTicket;
        }
    },


    getLocation:function(){
        var that = this;
        if(this.globalData.isSaveLocation){ // 已经上报地理位置不用重复上报
            return; 
        }
        
        wx.getSetting({
          success(res) {
            if (res.authSetting['scope.userLocation']) {
                wx.getLocation({
                  type: 'wgs84',
                  success: function(res) {
                    var latitude = res.latitude;
                    var longitude = res.longitude;
                    var vipInfo = that.globalData.vipInfo;
                    var lnglat = longitude+","+latitude;
                    wx.request({
                        url: that.globalData.httpRequest + '/api/pai/v2/user/my_city',
                        data:{lnglat:lnglat},
                        header: {
                            'content-type': 'application/x-www-form-urlencoded'
                        },
                        success: function(res) {
                          var status = res.data.status;
                          var nowCity = "";
                          var province = "";
                          if(status == 0){
                            nowCity = res.data.nowCity;
                            province = res.data.province;
                            that.saveLocation(province,nowCity);
                          }else if(status == 1){
                            var desc = res.data.desc;
                            util.showToast(desc,"none");
                          }
                        },
                        fail: function() {
                            util.showToast(app.globalData.NETWORK_EXCEPTION,"none");
                        }
                    })
                },
                fail:function(res){
                  }
                })
            } else {
                wx.showModal({
                    title: '提示',
                    content: '是否开启地理位置授权，授权后可获得完整体验',
                    confirmText:'一键开启',
                    success: function(res) {
                        if (res.confirm) {
                            if(canUseSetting){
                                wx.navigateTo({
                                    url:'/pages/shop/authorizePage/authorizePage?flag=2'
                                })
                            }else{
                                wx.showModal({
                                    title: '提示',
                                    content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
                                  })
                                }
                           
                        } else if (res.cancel) {
                          
                        }
                    }
                }) 
            } 
          }
        })
    },


    init: function(successCb, failCb) {
        var that = this;
        this.loginIn(function(userInfo) {
            that.getVipInfo(userInfo.session_key, userInfo.encryptedData, userInfo.iv, successCb, failCb);
        }, failCb);
    },

    loginIn: function(successCb, failCb) {
        var that = this;
        if (this.globalData.userInfo) {
            
            typeof successCb == "function" && successCb(this.globalData.userInfo)
        } else {
            console.log('login');
            //调用登录接口
            wx.login({
                success: function(res) {
                    console.log(res)
                    that.getOpenId(res.code, function(info) {
                        that.getUserInfo(info, successCb, failCb);
                    }, failCb);
                },
                fail: function() {
                    console.log('login fail');
                    that.globalData.loginErrorStatus = 0;
                    typeof failCb == "function" && failCb()
                }
            })
        }
    },
    getOpenId: function(code, successCb, failCb) {
        var that = this;
        wx.request({
            url: that.globalData.httpRequest + '/api/partner/jscode2session',
            data: { jsCode: code },
            header: {
                'content-type': 'application/x-www-form-urlencoded' // 默认值
            },
            method: 'post',
            success: function(res) {
                console.log('getOpenId success',res);
                if (res.data.status == 0) {
                    that.globalData.sessionKey = res.data.data.session_key;
                    typeof successCb == "function" && successCb(res.data.data)
                } else {
                    that.globalData.loginErrorStatus = 1;
                    typeof failCb == "function" && failCb()
                }
            },
            fail: function() {
                console.log('getOpenId fail');
                that.globalData.loginErrorStatus = 2;
                typeof failCb == "function" && failCb()
            }
        })
    },
    
    getUserInfo: function(info, successCb, failCb) {
        var that = this;
        wx.getSetting({
            success(res) {
                if (res.authSetting['scope.userInfo']) {
                    console.log("authSetting=====true")
                    wx.getUserInfo({
                        success: function(res) {
                            wx.checkSession({
                                success: function(){
                                    that.globalData.userInfo = res;
                                    that.globalData.userInfo.session_key = info.session_key;
                                    that.globalData.userInfo.openId = info.openid;
                                    that.globalData.userInfo.expires_in = info.expires_in;
                                    wx.setStorage({
                                        key:"userInfo",
                                        data:that.globalData.userInfo,
                                        success:function(){
                                            typeof successCb == "function" && successCb(that.globalData.userInfo)
                                        }
                                    })      
                                },
                                fail: function(){
                                    that.init();
                                }
                            })
                        },
                        fail: function() {
                            that.showAuthorizeModal(info, successCb, failCb);
                        }

                    });
                } else {
                    console.log("authSetting=====false")
                    that.showAuthorizeModal(info, successCb, failCb);
                }
            }
        })
    },

    showAuthorizeModal: function(info, successCb, failCb) {
        var that = this;
        wx.showModal({
            // title: '用户未授权',
            // content: '如需正常使用小程序功能，请按确定并在授权管理中选中“用户信息”，然后点按确定。最后再重新进入小程序即可正常使用。',
            title: '提示',
            content: '你还未登录，登录后可获得完整体验',
            confirmText:'一键登录',
            success: function(res) {
                if (res.confirm) {
                    //that.getAuthorize(info, successCb, failCb)
                    wx.navigateTo({
                        url:'/pages/authorizePage/authorizePage?flag=1'
                    })
                } else if (res.cancel) {
                    that.globalData.loginErrorStatus = 3;
                    typeof failCb == "function" && failCb()
                }
            }
        })
    },

    getAuthorize: function(info, successCb, failCb) {
        var that = this;
        wx.getSetting({
            success(res) {
                if (!res.authSetting['scope.userInfo']) {
                    wx.openSetting({
                        success() {
                            that.getUserInfo(info, successCb, failCb);
                        }
                    });
                } else {
                    that.getUserInfo(info, successCb, failCb);
                }
            }
        })
    },

    getVipInfo: function(sessionKey, encryptedData, iv, successCb, failCb) {
        var that = this;
        if (that.globalData.vipInfo) {
            typeof successCb == "function" && successCb(this.globalData.vipInfo)
        } else {
            wx.request({
                url: that.globalData.httpRequest + '/api/pai/decryptsession',
                data: { sessionKey: sessionKey, encryptedData: encryptedData, iv: iv },
                header: {
                    'content-type': 'application/x-www-form-urlencoded' // 默认值
                },
                method: 'post',
                success: function(res) {
                    console.log('getVipInfo success');
                    if (res.data.status == 0) {
                        var vipInfo = res.data.data;
                        that.globalData.vipInfo = vipInfo;
                        wx.setStorage({
                            key:"vipInfo",
                            data:vipInfo,
                            success:function(){
                                typeof successCb == "function" && successCb(vipInfo)
                            }
                        })                       
                    } else {
                        that.globalData.loginErrorStatus = 4;
                        typeof failCb == "function" && failCb();
                    }
                },
                fail: function() {
                    console.log('getVipInfo fail');
                    that.globalData.loginErrorStatus = 2;
                    typeof failCb == "function" && failCb();
                }
            })
        }
    },



    showErrorModal: function(msg, successCb, failCb) {
        wx.showModal({
            title: '提示',
            content: msg,
            showCancel: false,
            success: function(res) {
                if (res.confirm) {
                    typeof successCb == "function" && successCb()
                } else if (res.cancel) {
                    typeof failCb == "function" && failCb()
                }
            }
        })
    },

    setGlobalData: function(propertyName, data) {
        this.globalData[propertyName] = data;
    },

 
    getUserInfoCache:function(){
        var that = this;
        wx.getStorage({
            key:"userInfo",
            success:function(res){
                that.setGlobalData("userInfo",res.data);
            }
        })
    },

    getVipInfoCache:function(){
        var that = this;
        wx.getStorage({
            key:"vipInfo",
            success:function(res){
                that.setGlobalData("vipInfo",res.data);
            }
        })
    },

    judgeIphoneX:function(){
        var that = this;
        wx.getSystemInfo({
          success: function(res) {
            var name = 'iPhone X'
            if(res.model.indexOf(name) > -1){
              that.globalData.isIpx = true
            }
          }
        })
    },
    
  globalData: {
    loginErrorStatus: -1, // 0 :登录失败,1:获取openId失败,2:网络异常,3:获取小程序用户信息失败,4:获取会员手机号失败
    sessionKey:"", 
    userInfo: null,
    vipInfo: null,
    imgPrex: "http://img.bebeauty.cc/",
    //httpRequest: 'https://app.bebeauty.cc',
    // httpRequest: "https://app.51mrpp.com",
    httpRequest: "https://tt.51mrpp.com",
    httpRequest_1: "https://shop.51mrpp.com",
    GET_VIPINFO_FAIL: '获取用户信息失败，请重试',
    NETWORK_EXCEPTION: '网络异常，请重试',
    machineCheckKey: "MACHINEREPLCHECK",
    machineReplItemKey: "MACHINEREPLITEM",
    machineProductKey: "MACHINEPRODUCT",
    machineAddress: null,
    shareTicket: ""
  }
})